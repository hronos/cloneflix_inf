# -*- mode: ruby -*-
# vi: set ft=ruby :

# ----------------------------------------------------------------------------
# This vargant configuration is for:
# - Main management vm
#
# Vagrant box : Clean CentOS 7 x86_64
# Provisioning: Shell scripts + ansible
# ----------------------------------------------------------------------------

# Full documentation is here:
# http://docs.vagrantup.com/v2/

Vagrant.configure('2') do |config|
  # VM - Asible (admin server)
  config.vbguest.auto_update = true
  config.vm.define :admin do |admin|
    admin.vm.box = 'centos7.box'
    admin.vm.box_url = 'https://atlas.hashicorp.com/geerlingguy/boxes/centos6/versions/1.0.9/providers/virtualbox.box'
    admin.vm.hostname = 'admin'

    # --- Hardware configuration for VirtualBox ---
    admin.vm.provider 'virtualbox' do |vb|
      vb.memory = 512
    end

    # --- Internal network only ---
    # This interface will be used for connecting with others VMs via internal network
    admin.vm.network :private_network, ip: '192.168.77.100'

    # Forwarding ports
    # - Normal HTTP and HTTPS
    admin.vm.network :forwarded_port, guest: 80,  host: 10080
    admin.vm.network :forwarded_port, guest: 443, host: 10443

    # AUTODOC: START
    # Shared folders
    # Mount folders related with admin project
    # AUTODOC: END

    admin.vm.synced_folder '../provisioning', '/src/admin/provisioning'
    admin.vm.synced_folder '../cfg', '/src/admin/cfg'
    admin.vm.synced_folder '../..', '/src/project'
    admin.vm.synced_folder '../ansible', '/src/ansible'


    # Basic provisioning - run shell scripts for specific tasks.

    admin.vm.provision :shell, path: '../provisioning/bootstrap-epel.sh'
    admin.vm.provision :shell, path: '../provisioning/bootstrap-initial.sh'
    admin.vm.provision :shell, path: '../provisioning/bootstrap-ansible.sh'
    
  end
end
