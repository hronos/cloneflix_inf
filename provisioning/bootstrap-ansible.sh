#!/bin/bash
# ----------------------------------------------------------------------
# 2015-09
#
# Basic shell script to install Yum EPEL repo.
# Used for bootstrapping vagrant VMs
# ----------------------------------------------------------------------

set -e


# -- GLOBAL VARIABLES --
REQUIRED_PKG="python-setuptools wget"
ANSIBLE_PKG="ansible"
ANSIBLE_REQ_MIN_VERSION="1.9.4"

# Install required packages
function install_req_pkgs() {
	echo "Installing required packages for ansible..."
	yum -y install ${REQUIRED_PKG}
}

# Install ansible from LogicNow repository.
function install_ansible() {
	
	# Install ansible with yum to automatically download dependencies
	yum -y install ansible
}

# Check version for ansible, is important that ansible is in valid version
function ansible_check_version() {
	local ansible_ver=`ansible --version | cut -d" " -f 2`
	
	if [[ "${ansible_ver}" < "${ANSIBLE_REQ_MIN_VERSION}" ]]; then
		echo ""
		echo "ERROR: At least ansible '${ANSIBLE_REQ_MIN_VERSION}' version must be installed"
		echo "  Installed: ${ansible_ver}"
		echo "  Required : ${ANSIBLE_REQ_MIN_VERSION}"
		echo ""
		echo "Please check whether proper EPEL repository is installed."
		echo "or re-run bootstrap-epel.sh' script and re-run this script after."
		echo ""
		exit 2
	fi
}

# Configure /etc/hosts
function hosts_configure() {
	echo "Configuring /etc/hosts to have access to other vms"
	if ! grep --quiet "admin.dlennart.co.uk" /etc/hosts; then
		echo "192.168.77.100 admin.dlennart.co.uk" >>/etc/hosts
	fi
}

# Configure ansible
function ansible_configure() {
    echo "Configuring ansible"
    cp -vf /src/admin/cfg/ansible.cfg /etc/ansible/
}

# --- MAIN CODE ---
hosts_configure
install_req_pkgs
install_ansible
ansible_check_version
ansible_configure

echo ""
echo "=========================="
echo "Installed Ansible version:"
ansible --version
echo "=========================="
echo ""

exit 0
