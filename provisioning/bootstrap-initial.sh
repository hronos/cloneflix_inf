#!/bin/bash
# ----------------------------------------------------------------------
# 2015-09
#
# Basic shell script to install Yum EPEL repo.
# Used for bootstrapping vagrant VMs
# ----------------------------------------------------------------------

set -e

# --- Functions ---
function detect_system() {
    local release="/etc/redhat-release"
    if [ -f "${release}" ]; then
        SYSTEM=`cat ${release}`
        RELEASE_FILE="${release}"
    fi
}


# Configure firewall 
function firewall_config() {
	echo "Configuring Firewall"
	# Remove firewalld
	service iptables stop
    chkconfig iptables off
}


detect_system
firewall_config

exit 0

