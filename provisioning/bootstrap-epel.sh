#!/bin/bash
# ----------------------------------------------------------------------
# 2015-09
#
# Basic shell script to install Yum EPEL repo.
# Used for bootstrapping vagrant VMs
# ----------------------------------------------------------------------

set -e

yum install -y  wget

# -- GLOBAL VARIABLES --
FILE_RELEASE_RH="/etc/redhat-release"
ASSET_OPERATING_SYSTEM=""

# Check operating system - detecting CENT OS so far
function check_operating_system() {
    local valid_system=1

    if [ -f "${FILE_RELEASE_RH}" ]; then
		# Expected result: CentOS_release_7.X or Red_Hat_Enterprise_Linux_Server_release_7.X
		ASSET_OPERATING_SYSTEM=`head -n 1 "${FILE_RELEASE_RH}" | sed -e "s/ (.*)$//g" -e "s/\.[0-9]$/.X/g" | tr " " "_"`
		valid_system=0
        echo $ASSET_OPERATING_SYSTEM
		fi

    if [ "${valid_system}" = "1" ]; then
		echo "ERROR: Can't determine system. Detecting so far: CentOS"
    fi

    return ${valid_system}
}

# Update ca-certs.
# This is required to allow install others repositories (like EPEL).
# During time CA-certificates changes and valid certs should be available
# from the begining of provsioning.
function update_ca_certs() {
    echo "-> Updating ca-certificates to latest version..."
    yum clean all
    # When EPEL is already installed - run:
    # yum -y upgrade --disablerepo=epel --disableplugin=fastestmirror ca-certificates 
    yum -y upgrade --disableplugin=fastestmirror ca-certificates 
}

# Install yum additional repo
function install_yum_repository() {
    local yumrepo="$1"
    local yumurl="$2"
    local rpm2chk="$3"

    echo " + ${yumrepo}:"
	set +e
    rpm -q --quiet ${rpm2chk}
    rc=$?
	set -e
    if [ ! $rc = 0 ]; then
        echo "  -> Installing..."
        rpm -ivh ${yumurl}
    else
        echo " -> Already installed, skipped..."
    fi

}

# Install additional YUM repos - depends of verios of OS
function install_yum_repos() {
    if check_operating_system; then
        arch=`uname -i`
            case ${arch} in
                i386)
                    url_epel="https://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm"
                     ;;
                x86_64)
                    url_epel="https://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm"
                    ;;
            esac


        # Install packages
        echo "Installing yum repositories..."
        install_yum_repository "EPEL" "${url_epel}" "epel-release"
    fi
}

# --- MAIN CODE ---
update_ca_certs
install_yum_repos

exit 0
